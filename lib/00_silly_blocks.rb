# Mike Brinkman
# 6/27/17

def reverser
  word_arr = yield.split(' ')
  new_arr = []

  word_arr.each { |w| new_arr << w.reverse }

  new_arr.join(' ')
end

def adder(n=1, &block)
  block.call + n
end

def repeater(n = 0, &block)
  return block.call if n == 0

  n.times do |num|
    block.call
  end
end
