
def measure(n=1, &block)
  start_time = Time.now

  n.times do
    block.call
  end

  end_time = Time.now
  if n > 1
    (end_time - start_time) / n
  else
    end_time - start_time
  end
end
